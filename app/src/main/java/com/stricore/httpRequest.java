package com.stricore;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;



public class httpRequest  extends AsyncTask<String, Void, String> {


    ProgressDialog p;
    Context context;

    public static final String URL="http://192.168.0.182:5000/graphql";

    public httpRequest(Context cont){
        context = cont;
    }

    @Override
    protected void onPostExecute(String result) {

    }



    @Override
    protected void onPreExecute() {
        //TODO show messages in operation
    }

    @Override
    protected void onProgressUpdate(Void... values) {

    }




    @Override
    protected String doInBackground(String... string)  {
        Log.e("LOG_RESPONSE", "doInbackgroud");


        final String[] result = {""};
        JSONObject jsonBody = new JSONObject();
        try{
            jsonBody.put("query", string[0]);
        }catch(Exception e){

        }
        final String mRequestBody = jsonBody.toString();
        RequestQueue queue = (RequestQueue) Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URL,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("LOG_RESPONSE", response.toString());


                        //TODO something
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_RESPONSE", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
        return result[0];

    }
}

