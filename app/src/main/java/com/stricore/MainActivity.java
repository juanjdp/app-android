package com.stricore;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.util.Log;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class MainActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this.getApplicationContext();
        Button button=findViewById(R.id.button);



        Log.d("LOG_RESPONSE","begin");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOG_RESPONSE","Inside click");
                String query = "{\n" +
                        "  userById(id: 2) {\n" +
                        "    username,\n" +
                        "    password\n" +
                        "  }\n" +
                        "}";
                httpRequest asyncTask=new httpRequest(context);
                asyncTask.execute(query);
            }
        });
    }
}
